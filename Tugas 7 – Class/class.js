console.log("1. Animal Class")
console.log("Release 0")
class Animal {
    constructor(nama) {
        this.name = nama
    }
    get legs() {
        return '4'
    }
    get cold_blooded() {
        return false
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("==========================\n")

console.log("Release 1")

class Ape extends Animal {
    yell() {
        return console.log('Auooo')
    }
}

class Frog extends Animal {
    jump() {
        return console.log('hop hop')
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("====================================\n")

console.log("2. Function to Class")
class Clock {
    constructor({ template }) {
        this.alur = template
    }
    render() {
        var alur = this.alur
        var date = new Date()
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = alur
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        return console.log(output);
    }

    
    start() {
        this.timer = setInterval(this.render.bind(this), 1000);
        return this.render();
    }
    stop() {
        clearInterval(this.timer);
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();