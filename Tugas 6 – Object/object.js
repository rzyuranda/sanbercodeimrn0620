console.log("Soal No. 1 (Array to Object")
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var jawab = {}
    var nama = {}
    for (var i = 0; i < arr.length; i++) {
        nama[i] = (arr[i].slice(0, 2)).join(" ")
        jawab[nama[i]] = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: thisYear - (arr[i][3])
        }
        if (arr[i][3] == null || arr[i][3] > thisYear) {
            jawab[nama[i]].age = "Invalid Birth Year"
        }
    }
    return console.log(jawab)
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""
console.log("===============\n")
console.log("Soal No. 2 (Shopping Time)")
function shoppingTime(memberId, money) {
    var jawab
    var kembalian = 0;
    var belanjaan = []
    var totalharga = 0;
    var barang = ['Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone']
    var harga = [1500000, 500000, 250000, 175000, 50000]
    if (!memberId) {
        jawab = "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        jawab = "Mohon maaf, uang tidak cukup"
    } else {
        for (var i = 0; i < barang.length; i++) {
            if (money >= harga[i]) {
                totalharga = totalharga + harga[i]
                kembalian = money - totalharga
                belanjaan.push(barang[i])
            }
        }
        var jawab = {}
        jawab.memberId = memberId
        jawab.money = money
        jawab.listPurchased = belanjaan
        jawab.changeMoney = kembalian
    }
    return jawab
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("\n\n")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var kembalian = 0;
    var nama = {}
    var jawab = []

    var counts = {};

    for (var i = 0; i < rute.length; i++) {
        counts[rute[i]] = i;
    }
    for (var i = 0; i < arrPenumpang.length; i++) {
        var subArray = arrPenumpang[i],
            harusbayar = (counts[subArray[2]] - counts[subArray[1]]) * 2000
        item = {
            penumpang: subArray[0],
            naikdari: subArray[1],
            tujuan: subArray[2],
            bayar: harusbayar
        };
        jawab.push(item);

    }
    return jawab
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
