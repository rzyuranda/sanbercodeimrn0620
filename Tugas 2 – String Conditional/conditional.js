// If-else
var nama = "Abu";
var peran = "Werewolf";
if(nama == '' && peran == '') {
	// Input nama = '' dan peran = ''
	console.log("Nama harus diisi!");
} else {
	if(peran == '') {
		// Input nama = 'John' dan peran = ''
		console.log("Halo " + nama +", Pilih peranmu untuk memulai game!")
	} else {		
		if(peran == 'Penyihir') {
			// nama = 'Jane' dan peran 'Penyihir'
			console.log("Selamat datang di Dunia Werewolf, " + nama)
			console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
		} else if (peran == 'Guard') {
			// Input nama = 'Jenita' dan peran 'Guard'
			console.log("Selamat datang di Dunia Werewolf, " + nama)
			console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
		} else if (peran == 'Werewolf') {
			// Input nama = 'Junaedi' dan peran 'Werewolf'
			console.log("Selamat datang di Dunia Werewolf, " + nama)
			console.log("Halo Werewolf "+ nama +", Kamu akan memakan mangsa setiap malam!" )
		} //else {
			// jika peran tidak ada
		//	console.log("Halo " + nama +", Pilih peranmu untuk memulai game!")
		//}
	}
}

console.log("=====================================\n")
// Switch Case
var tanggal = 21;
var bulan = 5;
var tahun = 2019;
switch(bulan) {
	case 1 : { bulan = "Fanuari"; break; }
	case 2 : { bulan = "Februari"; break; }
	case 3 : { bulan = "Maret"; break; }
	case 4 : { bulan = "April"; break; }
	case 5 : { bulan = "Mei"; break; }
	case 6 : { bulan = "Juni"; break; }
	case 7 : { bulan = "Juli"; break; }
	case 8 : { bulan = "Agustus"; break; }
	case 9 : { bulan = "September"; break; }
	case 10 : { bulan = "Oktober"; break; }
	case 11 : { bulan = "November"; break; }
	case 12 : { bulan = "Desember"; break; }
	default : { bulan = "bulan tidak valid"}
}
console.log(tanggal.toString().concat(" ", bulan, " ", tahun))
// console.log(tanggal + " " + bulan + " " + tahun);