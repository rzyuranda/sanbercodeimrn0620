console.log("Soal No. 1 (Range) ")
function range(num1 = 0, num2 = 0) {
    var jawaban = [];
    if (num1 == 0 || num2 == 0) {
        // jika num1 atau nim2 kosong 
        jawaban.push(-1)
    } else if (num1 > num2) {
        // jika num 1 lebih besar dari num 2
        for (var i = num1; i >= num2; i += -1) {
            jawaban.push(i)
        }
    } else {
        // selain itu (default num2 lebih besar dari num 1)
        for (var i = num1; i <= num2; i += 1) {
            jawaban.push(i)
        }
    }
    return jawaban;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("=======================================\n")

console.log("Soal No. 2 (Range with Step)");

function rangeWithStep(num1 = 0, num2 = 0, num3 = 0) {
    var jawaban = [];
    if (num1 > num2) {
        // jika num 1 lebih besar dari num 2
        for (var i = num1; i >= num2; i -= num3) {
            jawaban.push(i)
        }
    } else {
        // selain itu (default num2 lebih besar dari num 1)
        for (var i = num1; i <= num2; i += num3) {
            jawaban.push(i)
        }
    }


    return jawaban;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("=======================================\n")

console.log("Soal No. 3 (Sum of Range)")

function sum(num1 = 0, num2 = 0, num3 = 1) {
    var total = 0;
    var jawaban = []
    if (num1 > num2) {
        for (var i = num1; i >= num2; i -= num3) {
            jawaban.push(i)
        }
    } else {
        for (var i = num1; i <= num2; i += num3) {
            jawaban.push(i)
        }
    }

    for (var i = 0; i < jawaban.length; i++) {
        total += jawaban[i]
    }
    return total
}

console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())
console.log("=======================================\n")

console.log("Soal No. 4 (Array Multidimensi)")
function dataHandling() {
    var jawaban = '';
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
    for (var i = 0; i < input.length; i++) {
        jawaban += "Nomor Id: " + input[i][0] + "\n"
        jawaban += "Nama Lengkap: " + input[i][1] + "\n"
        jawaban += "TTL: " + input[i][2] + " " + input[i][3] + "\n"
        jawaban += "Hobi: " + input[i][4] + "\n\n"
    }
    return jawaban
}

console.log(dataHandling())
console.log("=======================================\n")

console.log("Soal No. 5 (Balik Kata)")
function balikKata(kata) {
    var kataS = kata
    var jawab = '' 
    for (let i = kata.length - 1; i >= 0; i--) {
        jawab = jawab + kataS[i];
    }

    return jawab;
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))
console.log("=======================================\n")

console.log("Soal No. 6 (Metode Array)")

function dataHandling2(input) {
    // ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
    var jawab = input;
    jawab.splice(1, 1, "Roman Alamsyah Elsharawy");
    jawab.splice(2, 1, "Provinsi Bandar Lampung");
    jawab.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(jawab);

    // var bulan = jawab.slice(3,4)
    var ttl = jawab[3]
    var bulan_array = ttl.split("/")
    var bulan_array2 = ttl.split("/")
    var bulan = bulan_array[1];
    var nama = jawab[1];
    switch(parseInt(bulan)) {
        case 01 : {bulan = "January"; break;}
        case 02 : {bulan = "February"; break;}
        case 03 : {bulan = "Maret"; break;}
        case 04 : {bulan = "April"; break;}
        case 05 : {bulan = "Mei"; break;}
        case 06 : {bulan = "Juni"; break;}
        case 07 : {bulan = "Juli"; break;}
        case 08 : {bulan = "Agustus"; break;}
        case 09 : {bulan = "September"; break;}
        case 10 : {bulan = "Oktober"; break;}
        case 11 : {bulan = "November"; break;}
        case 12 : {bulan = "Desember"; break;}
        default : { bulan = "bulan tidak valid"}
    }
    console.log(bulan);
    // sort
    bulan_array.sort((a,b)=>b-a) ; 
    console.log(bulan_array);
    // 21-05-1989
    console.log(bulan_array2.join("-"))
    // nama dibatas 15
    console.log(nama.slice(0,15));
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);