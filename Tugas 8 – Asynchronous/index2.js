var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

function janji(index, times) {
    if (index < books.length) {
        readBooksPromise(times, books[index])
            .then(times => {
                return index  + janji(index + 1, times)
            })
            .catch(times => {console.log(times)})
    }
}
janji(0, 10000)

// function janji(books) {
//     for (var i = 0; i < books.length; i++) {
//         readBooksPromise(10000, books[i])
//             .then(function (fulfilled) {
//                 console.log(fulfilled);
//             })
//             .catch(function () {
//                 // console.log(rej.message);
//                 // kayaknya ini enggak perlu
//                 // sudah di test waktu untu buku fidas diganti 20000
//                 // output = saya sudah tidak punya waktu untuk baca Fidas
//             });
//     }
// }
// janji(books)

// books.forEach(element => readBooksPromise(10000, element, (callbackFn) => {
//     console.log(callbackFn)
// }))