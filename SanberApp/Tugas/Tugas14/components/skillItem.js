import React from 'react';
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
export default class SkillScreen extends React.Component {
    render() {
        let skill = this.props.skill
        return (
            <View style={styles.container}>
                <View style={styles.skillbox}>
                    <Icon name={skill.iconName} size={80} style={{ color: '#003366', alignSelf: 'center' }} />
                    <View style={styles.kemajuan}>
                        <Text style={styles.judulSkill}>{skill.skillName}</Text>
                        <Text style={styles.kategoriskill} >{skill.categoryName}</Text>
                        <Text style={styles.Kemajuan}>{skill.percentageProgress}</Text>
                    </View>
                    <Icon name="chevron-right" size={100} style={{ color: '#003366', alignSelf: 'center' }} />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    skillbox: {
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        height: 129,
        flexDirection: 'row'
    },
    kemajuan: {
        alignSelf: 'center',
        marginLeft: 25
    }, 
    judulSkill: {
        fontSize: 24,
        color: '#003366',
    },
    kategoriskill: {
        fontSize: 16,
        color: '#3EC6FF'
    },
    Kemajuan: {
        fontSize: 48,
        color: '#FFFFFF',
        fontWeight: 'bold',
        alignSelf: 'flex-end'
    }
})