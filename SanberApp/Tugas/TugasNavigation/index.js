import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Login } from './LoginScreen';
import { About } from './AboutScreen';
import { Project } from './ProjectScreen';
import { Tambah } from './AddScreen';
import { Skill } from './SkillScreen';

const LoginStack = createStackNavigator();
const LoginStackScreen = () => (
    <LoginStack.Navigator>
        <LoginStack.Screen headerMode="none" name="Login" component={Login} options={{ title: "Login Page" }} />
    </LoginStack.Navigator>
)
const Tabs = createBottomTabNavigator();
const SkillStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const TambahStack = createStackNavigator();

const SkillStackScreen = () => (
    <SkillStack.Navigator>
        <SkillStack.Screen name="Skill" component={Skill} />
    </SkillStack.Navigator>
)
const ProjectStackScreen = () => (
    <ProjectStack.Navigator>
        <ProjectStack.Screen name="Project" component={Project} />
    </ProjectStack.Navigator>
)
const TambahStackScreen = () => (
    <TambahStack.Navigator>
        <TambahStack.Screen name="Tambah" component={Tambah} />
    </TambahStack.Navigator>
)

const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
    <AboutStack.Navigator>
        <AboutStack.Screen name="About" component={About} />
    </AboutStack.Navigator>
)

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillStackScreen} />
        <Tabs.Screen name="Project" component={ProjectStackScreen} />
        <Tabs.Screen name="Tambah" component={TambahStackScreen} />
    </Tabs.Navigator>
)

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="About" component={AboutStackScreen} />
        <Drawer.Screen name="Tabs" component={TabsScreen} />
    </Drawer.Navigator>
)

const RootStack = createStackNavigator();
const RootStackScreen = () => (
    <RootStack.Navigator>
        <RootStack.Screen name="Login" component={LoginStackScreen} />
        <RootStack.Screen name="App" component={DrawerScreen} />
    </RootStack.Navigator>
)

export default () => (
    <NavigationContainer>
        <RootStackScreen />
    </NavigationContainer>
);
