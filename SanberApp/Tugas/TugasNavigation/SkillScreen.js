import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SkillItem from '../Tugas14/components/skillItem';
import data from '../Tugas14/skillData.json';

export const Skill = () => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Image source={require('../Tugas13/assets/logo.png')} style={{ width: 185, height: 51 }} />
            </View>
            <View style={styles.dataDiri}>
                <Icon name="account-circle" size={40} style={{ color: '#3EC6FF' }} />
                <View style={styles.dataDiriItem}>
                    <Text style={{ fontSize: 12 }} >Hai,</Text>
                    <Text style={{ fontSize: 16, color: '#003366' }} >Rezky Yuranda</Text>
                </View>
            </View>
            <View style={styles.skillCategory}>
                <Text style={{ fontSize: 36, color: '#003366' }} >SKILL</Text>
                <View style={{ height: 4, backgroundColor: '#3EC6FF' }} />
                <View style={styles.kategori}>
                    <View style={styles.isiKategori}>
                        <Text style={styles.textKategori}>Library / Framework</Text>
                    </View>
                    <View style={styles.isiKategori}>
                        <Text style={styles.textKategori}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.isiKategori}>
                        <Text style={styles.textKategori}>Teknologi</Text>
                    </View>
                </View>
            </View>
            <View style={styles.body}>
                {/* <SkillItem skill={data.items[0]} /> */}
                <FlatList
                    data={data.items}
                    renderItem={(skill) => <SkillItem skill={skill.item} />}
                    keyExtractor={(items) => items.id}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    body: {
        flex: 1
    },
    header: {
        alignSelf: 'flex-end'
    },
    dataDiri: {
        marginTop: 3,
        marginLeft: 12,
        marginRight: 12,
        flexDirection: 'row'
    },
    dataDiriItem: {
        marginLeft: 11,
        flexDirection: 'column'
    },
    skillCategory: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 16
    },
    kategori: {
        marginTop: 10,
        flexDirection: 'row',
        // alignItems: 'center',
        // alignSelf: 'center'
    },
    isiKategori: {
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        marginRight: 4
    },
    textKategori: {
        fontSize: 12,
        color: '#003366',
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 9,
        paddingBottom: 9,
    }
})