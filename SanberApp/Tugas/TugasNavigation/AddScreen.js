import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
export const Tambah = () => {
    return (
        <View style={styles.container}>
            <Text>Halaman Tambah</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'center',
        marginTop: 230
    }
})