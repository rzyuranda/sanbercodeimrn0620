import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {     
	StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity } from 'react-native';


export const About = () => {
    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.AtasBck}>
                    <Text style={styles.judul}>Tentang saya</Text>
                    <Ionicons style={styles.navIcon} name="ios-contact" size={220} color="#EFEFEF" />
                    <Text style={styles.nama}>Rezky Yuranda</Text>
                    <Text style={styles.jobdesk}>React Native Developer</Text>
                </View>
                <View style={styles.portofolio}>
                    <View style={{ marginLeft: 14 }}>
                        <Text style={{ marginTop: 8, fontSize: 18, color: '#003366' }}>Portfolio</Text>
                        <View style={{ height: 1, backgroundColor: '#003366', marginTop: 6 }} />
                    </View>
                    <View style={styles.git}>
                        <View style={styles.gitItem}>
                            <Ionicons name="logo-github" size={50} color="#3EC6FF" />
                            <Text style={styles.textGit}>@rzyuranda</Text>
                        </View>
                        <View style={{ width: 65 }}></View>
                        <View style={styles.gitItem}>
                            <Ionicons name="logo-github" size={50} color="#3EC6FF" />
                            <Text style={styles.textGit}>@rzyuranda</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.huBungiSaya}>
                    <View style={{ marginLeft: 14 }}>
                        <Text style={{ marginTop: 8, fontSize: 18, color: '#003366' }}>Hubungi saya</Text>
                        <View style={{ height: 1, backgroundColor: '#003366', marginTop: 6 }} />
                    </View>
                    <View style={styles.sosmed}>
                        <View style={styles.sosmedRow}>
                            <Ionicons name="logo-facebook" size={50} color="#3EC6FF" />
                            <Text style={styles.textSosmed}>@rzyuranda</Text>
                        </View>
                        <View style={styles.sosmedRow}>
                            <Ionicons name="logo-instagram" size={50} color="#3EC6FF" />
                            <Text style={styles.textSosmed}>@rzyuranda</Text>
                        </View>
                        <View style={styles.sosmedRow}>
                            <Ionicons name="logo-twitter" size={50} color="#3EC6FF" />
                            <Text style={styles.textSosmed}>@rzyuranda</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    AtasBck: {
        alignItems: 'center'
    },
    judul: {
        marginTop: 64,
        fontWeight: 'bold',
        fontSize: 36,
        color: '#003366'
    },
    navIcon: {
        marginTop: 12
    },
    nama: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#003366'
    },
    jobdesk: {
        marginTop: 8,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3EC6FF'
    },
    portofolio: {
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        marginLeft: 8,
        marginRight: 8,
        marginTop: 16
    },
    git: {
        marginTop: 16,
        marginBottom: 16,
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'space-around'
    },
    gitItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textGit: {
        marginTop: 10,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#003366'
    },
    huBungiSaya: {
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        marginLeft: 8,
        marginRight: 8,
        marginTop: 16,
        marginBottom: 16
    },
    sosmed: {
        marginTop: 16,
        marginBottom: 16,
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'space-around'
    },
    sosmedRow: {
        marginBottom:19,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textSosmed: {
        marginLeft: 18,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#003366'
    }

})