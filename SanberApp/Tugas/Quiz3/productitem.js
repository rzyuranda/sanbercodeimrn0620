import React from 'react';
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
export default class productitem extends React.Component {
    render() {
        let produk = this.props.barang
        // alert(this.props.barang)
        return (
            <View style={styles.container}>
                <View style={styles.columns}>
                    <View style={styles.boxList}>
                    <Image source={{uri: produk.gambaruri}}/>
                    <Text>{produk.nama}</Text>
                    <Text style={{color: 'blue', fontSize: 16}}>Rp. {produk.harga}</Text>
                    </View>
                    <View style={styles.boxList}>
                    <Image source={{uri: produk.gambaruri}}/>
                    <Text>{produk.nama}</Text>
                    <Text style={{color: 'blue', fontSize: 16}}>Rp. {produk.harga}</Text>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    columns: {
        flexDirection: 'row',       
    },
    boxList: {
        backgroundColor: '#ffffff',
        padding:4,
        margin:4,
        flexDirection: 'column',
        alignSelf: 'center',
        alignItems: 'center'
    }

    
})