import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class VideoItems extends Component {
    render() {
        let vidio = this.props.vidio;
        return (
            <View style={styles.container}>
                <Image source={{ uri: vidio.snippet.thumbnails.medium.url }} style={{ height: 200 }} />
                <View style={styles.desCont}>
                    <Image source={{ uri: 'https://randomuser.me/api/portraits/men/0.jpg' }} style={{ width: 50, height: 50, borderRadius: 25 }} />
                    <View style={styles.vidioDetails}>
                        <Text numberOfLines={2} style={styles.vidioTitles}>{vidio.snippet.title}</Text>
                        <Text style={styles.vidioStats}>{vidio.snippet.channelTitle + " - " + nFormatter(vidio.statistics.viewCount,1) + ' - 3 mounts ago'}</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name="more-vert" size={20} color="#999999"/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

function nFormatter(num, digits) {
    var si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "k" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "G" },
        { value: 1E12, symbol: "T" },
        { value: 1E15, symbol: "P" },
        { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
            break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + ' views';
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
    },
    desCont: {
        flexDirection: 'row',
        paddingTop: 15
    },
    vidioDetails: {
        paddingHorizontal: 15,
        flex: 1
    },
    vidioTitles: {
        fontSize: 16,
        color: '#3c3c3c'
    },
    vidioStats: {
        fontSize: 14,
        color: '#666565'
    }

})