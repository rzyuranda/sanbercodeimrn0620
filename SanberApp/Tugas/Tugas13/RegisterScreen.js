import React from 'react';
    import {
        StyleSheet,
        Text,
        View,
        Image,
        TextInput,
        ScrollView,
        TouchableOpacity
    } from 'react-native';

export default function LoginScreen() {
    return (
        <View style={styles.container}>
            <ScrollView style={styles.scrollView}>
                <View style={styles.logo}>
                    <Image source={require('./assets/logo.png')} style={{ width: 375, height: 102 }} />
                </View>
                <View style={styles.register}>
                    <Text style={{ fontSize: 24, color: '#003366' }}>Register</Text>
                </View>
                <View style={styles.FormInput}>
                    <View style={{ marginBottom: 16 }}>
                        <Text style={{ fontSize: 16, color: '#003366' }}>Username</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={{ marginBottom: 16 }}>
                        <Text style={{ fontSize: 16, color: '#003366' }}>Email</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={{ marginBottom: 16 }}>
                        <Text style={{ fontSize: 16, color: '#003366' }}>Password</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={{ marginBottom: 16 }}>
                        <Text style={{ fontSize: 16, color: '#003366' }}>Ulangi Password</Text>
                        <TextInput style={styles.input} />
                    </View>
                </View>
                <View style={{ alignItems: 'center', paddingTop: 32, paddingBottom:45 }}>
                    <View style={styles.buttonX} >
                        <TouchableOpacity>
                            <Text style={{ fontSize: 24, color: '#fff', textAlign: 'center' }}>Daftar</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ paddingTop: 16 }}>
                        <Text style={{ color: '#3EC6FF', fontSize: 24 }} >atau</Text>
                    </View>
                    <View style={styles.buttonS} >
                        <TouchableOpacity style={{}}>
                            <Text style={{ fontSize: 24, color: '#fff' }}>Masuk ?</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    logo: {
        // height: 102,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        paddingTop: 63
    },
    register: {
        alignItems: 'center',
        marginTop: 70,
    },
    FormInput: {
        paddingTop: 38,
        paddingLeft: 41,
        paddingRight: 41
    },
    input: {
        height: 48,
        borderColor: '#003366',
        borderWidth: 1,
        backgroundColor: '#FFFFFF'
    },
    buttonS: {
        marginTop: 16,
        height: 40,
        width: 140,
        justifyContent: 'center', 
        backgroundColor: '#3EC6FF',
        alignSelf: 'center', 
        alignItems: 'center',        
        borderBottomLeftRadius: 16,
        borderBottomRightRadius: 16,
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16
    },
    buttonX: {
        marginTop: 16,
        height: 40,
        width: 140,
        justifyContent: 'center', 
        backgroundColor: '#003366',
        alignSelf: 'center', 
        alignItems: 'center',        
        borderBottomLeftRadius: 16,
        borderBottomRightRadius: 16,
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
    },
    scrollView: {
        backgroundColor: 'white',
    },

})