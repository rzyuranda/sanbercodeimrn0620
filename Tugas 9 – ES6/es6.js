console.log("Mengubah fungsi menjadi fungsi arrow")
const golden = () => {
    return console.log("this is golden!!")
}
golden()

console.log("=========================\n")

console.log("Sederhanakan menjadi Object literal di ES6")
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullname: () => {
            console.log(firstName + " " + lastName)
            return
        }
    }
}
newFunction("William", "Imoh").fullname()
console.log("=========================\n")

console.log("3. Destructuring")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject;

console.log(firstName, lastName, destination, occupation)
console.log("=========================\n")

console.log("4. Array Spreading")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west, ...east]
//Driver Code
console.log(combined)
console.log("=========================\n")

console.log("5. Template Literals")
const planet = "earth"
const view = "glass"
var before = `Lorem ${planet} dolor sit amet, ` +
    `consectetur adipiscing elit, ${view} do eiusmod tempor ` +
    `incididunt ut labore et dolore magna aliqua. Ut enim` +
    ` ad minim veniam`

// Driver Code
console.log(before) 