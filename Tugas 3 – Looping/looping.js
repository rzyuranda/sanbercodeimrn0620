console.log("No. 1 Looping While\n");
// cara 1
// console.log("LOOPING PERTAMA");
// var while1 = 2;
// while(while1 <= 20) {
//   console.log(while1 + " - I love coding");  
//   while1 = while1 + 2;
// } 

// console.log("LOOPING KEDUA");
// var while2 = 20;
// while(while2 >= 2) {
//   console.log(while2 + " - I will become a mobile developer");  
//   while2 = while2 - 2;
// } 
// cara 2
var while1 = 2;
var while2 = 20
console.log('LOOPING PERTAMA');
while(while1 <= 20 && while2 >= 2) {
	console.log(while1 + " - I love coding"); 
	while1 = while1 + 2;  
	if(while1 > 20) {
		console.log('LOOPING KEDUA')
		while(while2 >= 2) {
			console.log(while2 + " - I will become a mobile developer");  
			while2 = while2 - 2;
		} 
	}
} 
console.log("\n")

console.log("No. 2 Looping menggunakan for");
for(var angka = 1; angka <= 20; angka++) {
	if((angka%3)==0 && (angka%2)==1 ) {
		// jika kelipatan 3 & ganjil
		console.log(angka + ' - I love coding');
	} else if ((angka%2)==1) {
		// jika ganjil
		console.log(angka + ' - Ganjil');
	} else if((angka%2)==0) {
		// jika genap
		console.log(angka + ' - Genap');
	}
}
console.log("\n")

console.log("No. 3 Membuat Persegi Panjang")
var persegi = '';
// perulangan jumlah (tinggi)
for (var i = 1; i <= 4; i++) {
	// perulangan lebar = 8
	for (var k = 1; k <= 8; k++) {
		persegi += "#";
	}
	persegi+= '\n';
}
console.log(persegi);

console.log("No. 4 Membuat Tangga");
var tangga = '';
// jumlah baris
for (var i = 1; i <= 7; i++) {	
	for (var k = i; k <= i; k++) {
		tangga += "#";
	}
	for (var l = 1; l <= i-1; l++) {
		tangga += '#';
	}
	tangga+= '\n';
}
console.log(tangga)

console.log("No. 5 Membuat Papan Catur");
var catur = '';
// perulangan jumlah tinggi (baris)
for (var i = 1; i <= 8; i++) {
	// kondisi, jika ganjil
	if((i%2)==1) {
		// kasih spasi
		catur += ' ';
	}	
	// jumlah # (lebar)
	for (var k = 1; k <= 4; k++) {
		catur += "# ";
	}
	catur+= '\n';
}
console.log(catur)